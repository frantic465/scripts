#!/usr/bin/env python

import sys
import subprocess
from collections import namedtuple
import re
import os

MOUNT_ROOT = '/mnt/'
FSTAB_LINE_RE = re.compile(r'''
                           (?P<path>\S+)\s+         # path to device
                           (?P<boot>\*)?\s+         # bootable flag
                           (?P<start>\d+)\s+        # start block
                           (?P<end>\d+)\s+          # end block
                           (?P<size>\d+)\s+         # block count
                           (?P<id>[0-9a-fA-F]+)\s+  # device' id
                           (?P<system>.*)           # suggested OS installed
                           ''', re.VERBOSE)

#Device = namedtuple('Device', ['name', 'desc', 'mount_point', 'path'])
class Device(object):
    def __init__(self, path, desc):
        self.path = path
        self.desc = desc

    @property
    def name(self):
        return os.path.basename(self.path)

    @property
    def mount_point(self):
        return os.path.join(MOUNT_ROOT, self.name)



def enum_devices():
    out = subprocess.check_output(['fdisk', '-l'])
    devices = []
    for line in out.splitlines():
        m = FSTAB_LINE_RE.match(line)
        if m:
            devices.append(Device(m.group('path'), m.group('system')))
    return devices


def choose_device(devices):
    for i, dev in enumerate(devices):
        print '{i}:{dev.path}\t{dev.desc}'.format(i=i, dev=dev)

    index = int(raw_input('\nChoose device 0..{}\n'.format(len(devices) - 1)))
    return devices[index]


def check_device_and_mount_point(device):
    out = subprocess.check_output(['mount'])
    for line in out.splitlines():
        if device.path in line:
            raise Exception('Device {!r} is already mounted'.format(device.path))
        if device.mount_point in line:
            raise Exception('Mount point {!r} is already used'.format(device.mount_point))


def prepare_mount_point(device):
    if os.path.exists(device.mount_point):
        if not os.path.isdir(device.mount_point):
            raise Exception('Can not use mount point {!r}. It\'s not a directory'.format(device.mount_point))
        if len(os.listdir(device.mount_point)) != 0:
            raise Exception('Can not use mount point {!r}. Directory is not empty'.format(device.mount_point))
    else:
        os.makedirs(device.mount_point)


def mount_device(device):
    print 'Mount {} on {!r}'.format(device.path, device.mount_point)
    subprocess.check_call(['mount', device.path, device.mount_point, '-t', 'auto'])


def main():
    print "Scan devices..."
    try:
        assert os.geteuid() == 0, 'Super user privileges are required'
        devices = enum_devices()
        selected = choose_device(devices)
        check_device_and_mount_point(selected)
        prepare_mount_point(selected)
        mount_device(selected)
    except Exception as e:
        print >> sys.stderr, e
        exit(-1)


if __name__ == "__main__":
    main()
